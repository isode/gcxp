Copyright
=========

This work is copyright [Isode Limited](https://isode.com) and made available as follows:

```
Copyright 2020 Isode Limited.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

Contributors
------------

None (yet). See [Contributing Guidelines](./CONTRIBUTING.md).


Other notices
-------------

Others have copyright in portions of this work, including:

### Boost.Asio

Christopher M. Kohlhoff holds copyright in [Boost.Asio](http://www.boost.org/doc/libs/release/doc/html/boost_asio.html) examples, fragments of which may be present in this work.

```
Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)

Distributed under the Boost Software License, Version 1.0. (See accompanying
file BOOST_LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
```

A verbatim copy of the [Boost Software License, Version 1.0](http://www.boost.org/LICENSE_1_0.txt) is provided in the file [BOOST_LICENSE_1_0.txt](3rdParty/BOOST_LICENSE_1_0.txt).
