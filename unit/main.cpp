// This file is part of GCXP which is copyright Isode Limited
// and others and released under a MIT license. For details, see the
// COPYRIGHT.md file in the top-level folder of the GCXP software
// distribution.
#define BOOST_TEST_MODULE GCXP unit tests
#include <boost/test/unit_test.hpp>
